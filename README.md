<h1 align="center">Form Generator Antdv</h1>

> Ant Design Vue 表单设计及代码生成器

---

## 简介

**Form Generator Antdv**，由[form-generator](https://github.com/JakHuang/form-generator) v0.2.0 搬运而来。form-generator 是一个优秀的 Element UI 表单设计及代码生成器，此项目将其改为 Ant Design Vue。

> 目前支持所有表单类组件(Data Entry)，支持布局类组件：Grid 栅格，通用类组件：Button 按钮。
>
> 可将生成的代码直接运行在基于 Ant Design Vue 的 vue 项目中；也可导出 JSON 表单，使用配套的解析器将 JSON 解析成真实的表单。

## JSON 解析器

将保存在数据库中的 JSON 表单，解析成真实的表单

```
// 安装
npm i form-gen-antdv-parser
```

## 开始使用

1. 环境准备

   - 安装[node](http://nodejs.org/)和[git](https://git-scm.com/)

2. 安装

   ```shell
   git clone
   ```

3. 本地开发

   进入项目根目录

   ```shell
   npm install
   ```

   > 若耗时太长可使用`npm install --registry=https://registry.npm.taobao.org`

   ```shell
   npm run dev
   ```

   > 打开浏览器访问 [http://localhost:8080](http://localhost:8080/)

4. 构建

   ```shell
   npm run build
   ```

## 文档

## 致谢

- [form-generator](https://github.com/JakHuang/form-generator) Element UI 表单设计及代码生成器
- [Ant Design Vue](https://github.com/vueComponent/ant-design-vue/) An enterprise-class UI components based on Ant Design and Vue
- [Ant Design Vue Pro](https://github.com/vueComponent/ant-design-vue-pro) Use Ant Design Vue like a Pro

## 联系

如果您发现了什么 bug，或者有什么界面建议或意见，

## 界面展示
